/* 
 * The MIT License
 *
 * Copyright(c) 2014 Marcelo Medina González.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


// DB class.
function DBCls(id, version){

    this.id = id;
    this.version = version;
    this.size = 0;
    this.creationDate = getFormatNow();
    this.tables = [];
    this.tablesIds = [];
    
};


// Interface for DB class
function DBItfc(db){

    this.getDBId = function(){return db.id;};
    this.getDBVersion = function(){return db.version;};
    this.getDBCreationDate = function(){return db.creationDate;};
    this.getDBSize = function(){return db.size;};
    this.getTablesIds = function(){return db.tablesIds;};
    this.getTablesNum = function(){return db.tablesIds.length;};

    function saveDB(db){localStorage.setItem( db.id, JSON.stringify(db) );};
    
    function existTable(id){return db.tablesIds.indexOf(id);};

    this.getTable = function(id){
        
        var ind = existTable(id);
        
        if(ind !== -1){
            
            return new TBItfc( db.tables[ind] );
            
        }
        
    };
    
    this.getTableInn = function(id){
        
        var ind = existTable(id);
        
        if(ind !== -1){
            
            return new TBItfcInn( db.tables[ind] );
            
        }
        
    };
    
    this.getAllTables = function(){
        
        var listTables = [];
        for(var i=0; i<db.tables.length; i++){
          
            listTables[i] = new TBItfc(db.tables[i]);
          
        }
        
        return listTables;
        
    };
    
    
    this.addTable = function(tb){
        
        if(existTable(tb.id) === -1){
            
            db.tables.push(tb);
            db.tablesIds.push(tb.id);
            saveDB(db);
            
        }
        
    };
    
    this.delTable = function(id){
        
        var ind = existTable(id);
        if(ind !== -1){
                    
            db.tables.splice(ind, 1);
            db.tablesIds.splice(ind, 1);    
            saveDB(db);
            
        }
        
    };

    this.updateTable = function(tb){

        var ind = existTable(tb.id);

        if(ind !== -1){
            
            db.tables[ind] = tb;
            saveDB(db);
            
        }
 
    };

};

