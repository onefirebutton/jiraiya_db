/* 
 * The MIT License
 *
 * Copyright(c) 2014 Marcelo Medina González.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



// Table class only data
function TBCls(id, title, description, columns){

    this.id = id;
    this.title = title;
    this.description = description;
    this.numRows = 0;
    this.colNames = [];
    this.colTypes = [];
    this.colNum = columns.length;

    for(var i=0; i<columns.length; i++){

        this.colNames.push(columns[i][0]);
        this.colTypes.push(columns[i][1]);

    }
    
    this.rows = [];

};

function TBItfc(tb){
    
     this.getTBId = function(){ return tb.id; };
     this.getTBTitle = function(){ return tb.title; };
     this.getTBDescription = function(){ return tb.description; };
     this.getTBNumRows = function(){ return tb.numRows; };
     this.getTBColNum = function(){ return tb.colNum; };
     this.getTBColTypes = function(){ return tb.colTypes; };
     this.getTBColNames = function(){ return tb.colNames; };
     
};


// 
function TBItfcInn(tb){
    
     this.getTBItfc = function(tb){ return new TBItfc(tb); };
     
     this.addRow = function(datos){ tb.rows.push( new RWCls(datos) ); tb.numRows += 1; return tb; };
     
     this.delRow = function(rowId){
         
         for(var i=0; i<tb.rows.length; i++){

             if(tb.rows[i].rowId === rowId){
                 
                 tb.rows.splice(i, 1);
                 tb.numRows -= 1;
                 return tb;
                 
             }
             
         }
         
         return tb; 
     };
     
     this.getAllRowIds = function(){ 
         
         var listIds = [];
         for(var i=0; i<tb.rows.length; i++){
             
             listIds.push(tb.rows[i].rowId);
             
         }   
         
         return listIds;
     };



};


