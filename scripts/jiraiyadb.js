/* 
 * The MIT License
 *
 * Copyright(c) 2014 Marcelo Medina González.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* */
function JirayaDB(selector){
            
    /* CONSTS */
    var DB_ID = 'jirayadb'; // web storage name
    var DB_VERSION = '0.0.9'; // jirayadb version



    /* FUNCTIONS */
    function getDB(){ return new DBItfc( JSON.parse( localStorage.getItem(DB_ID) ) ); };
    function getTableInn(id){ return getDB().getTableInn(id); };
    


    /* DATA BASE METHODS */
    this.createDB = function(rewrite){ ( rewrite === true || !existsInLS(DB_ID) ) ? 
                                         localStorage.setItem( DB_ID, JSON.stringify( new DBCls(DB_ID, DB_VERSION) ) ) : ""; };

    this.deleteDB = function(){ localStorage.removeItem(DB_ID); };
    
    this.getDBId = function(){ return getDB().getDBId(); };
    
    this.getDBVersion = function(){ return getDB().getDBVersion(); };

    this.getDBSize = function(){ return getDB().getDBSize(); };
     
    this.getDBCreationDate = function(){ return getDB().getDBCreationDate(); };
    
    
    /* TABLE METHODS */   
    this.addTable = function(id, title, description, columnas){ getDB().addTable( new TBCls(id, title, description, columnas) ); };

    this.delTable = function(id){ getDB().delTable(id); };
    
    this.getTablesIds = function(){ return getDB().getTablesIds(); };
    
    this.getTablesNum = function(){ return getDB().getTablesNum(); };
    
    this.getTable = function(id){ return getDB().getTable(id); };
    
    this.getAllTables = function(){ return getDB().getAllTables(); };
    
    /* ROWS METHODS */

    this.addRow = function(datos){ getDB().updateTable( getTableInn(selector).addRow(datos) ); };
    
    this.delRow = function(rowId){ getDB().updateTable( getTableInn(selector).delRow(rowId) ); };
    
    this.getAllRowIds = function(){ return getTableInn(selector).getAllRowIds(); };
   
    
        
}



/* Return an instance of JirayaDB  */
function jry(selector){return new JirayaDB(selector);}