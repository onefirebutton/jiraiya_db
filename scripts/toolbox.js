/* 
 * The MIT License
 *
 * Copyright(c) 2014 Marcelo Medina González.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function toolBox(){
    
    var contadorId = 0;
    
    this.getNewId = function(){ return ( new Date() ).valueOf() + '_' + contadorId++; };
    
    
    
}

var theToolBox = new toolBox();

// pasar todas las funciones a la clase toolBox
function getFormatNow(){
    
    var now = new Date();   
    var formated = '';
    
    formated += ( String( now.getDate() ).length === 1 ? '0' + now.getDate() : now.getDate() ) + '/';      
    formated += ( String( now.getMonth() + 1 ).length === 1 ? '0' + (now.getMonth() + 1) : now.getMonth() + 1 ) + '/';
    formated += now.getFullYear() + '/ - '; 
    formated += ( String( now.getHours() ).length === 1 ? '0' + now.getHours() : now.getHours() ) + ':'; 
    formated += ( String( now.getMinutes() ).length === 1 ? '0' + now.getMinutes() : now.getMinutes() ) + ':'; 
    formated += ( String( now.getSeconds() ).length === 1 ? '0' + now.getSeconds() : now.getSeconds() ); 
 
    
    return formated;
   
}

function existsInLS(key){
    
    for(var i=0; i<localStorage.length; i++){
        
        if( localStorage.key(i) === key){
            return true;
        }
        
    }
    
    return false;
    
}


 /*if(description !== null && description !== undefined){
        this.description = description;
    }*/